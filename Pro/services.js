const {
    query
} = require('../data');


const saveuser = async (email, first_name, username, passw, GDPR) => {
    await query(
      'INSERT INTO public.utilizator(customer_id, email, first_name, username, passw, rol, "GDPR") VALUES ((SELECT MAX(customer_id)+1 FROM public.utilizator), $1, $2, $3, $4,"user", 0)',
      [email, first_name, username, passw, GDPR]
    );
};



const updateRoleById = async (customer_id, rol) => {
    await query('UPDATE public.utilizator SET rol=$1 WHERE customer_id = $2', [rol, customer_id]);
};




const showUser = async (rol) => {
    return await query(
      'SELECT * FROM public.utilizator WHERE rol = $1;',
      [rol]
    );
};


const deleteById = async (customer_id) => {
    await query('DELETE FROM public.utilizator WHERE customer_id = $1', [customer_id]);
};


const updateUser = async (customer_id, email, first_name, username, rol, "GDPR") => {
    await query('UPDATE public.utilizator SET email = $1, first_name = $2, username = $3, rol = $4, "GDPR" = $5 WHERE customer_id = $6', [email, first_name, username, rol, "GDPR", customer_id]);
};



const updatePassById = async (customer_id, passw) => {
    await query('UPDATE public.utilizator SET passw=$1 WHERE customer_id = $2', [passw, customer_id]);
};



module.exports = {
    saveuser,
    updateRoleById,
    showUser,
    deleteById,
    updateUser,
    updatePassById

}
