const express = require('express');

const UtilizatorService = require('./services.js');
const {
    validateFields
} = require('../utils');
const {
    ServerError
} = require('../errors');

const router = express.Router();

const {
    authorizeRoles
} = require('../security/Roles');

const {
    authorizeAndExtractToken
} = require('../security/Jwt');



  /*  // CRUD = Create, Read, Update, Delete

     1. [CREATE] Functie creare user
      -> default toti userii sunt rol = 'user'

     2. [UPDATE] Setare suport tehnic / admin
         -> modificam rolul unui user
         -> primesc id-ul unui user si ii schimb rolul

     3. [READ] Vizualizare lista de useri
       a. vizualizare filtrare dupa Rol


     4.[DELETE] Stergere useri by id

     5. [UPDATE] Editare user
       -> fara parola

      6. [UPDATE] Schimbare parola

*/

// 1. (/utilizator/saveuser) [Create] Salvam userul in BD
// gdpr-ul era intre ghilimele; le-am sters si acum apare cu portocaliu
// id-ul nu l-am mai trecut
// rol trebuia sa fie aici by default user , deci il mai trec?
router.post('/saveuser', async (req, res, next) => {
    const {
      email,
      first_name,
      username,
      passw,
      GDPR
    } = req.body;

    try {
        await UtilizatorService.saveuser(email, first_name, username, passw, GDPR);

        res.status(201).end();
    } catch (err) {
        next(err);
    }
});




//2
router.put('/updaterol/:customer_id', async (req, res, next) => {
    const {
      customer_id
    } = req.params;
    const {
        rol
    } = req.body;

    try {

        await UtilizatorService.updateRoleById(parseInt(customer_id), rol);
        res.status(204).end();
    } catch (err) {
        next(err);
    }
});




// 3
router.get('/showuser/:rol', async (req, res, next) => {
    try {
        const {
            rol
        } = req.params;

        const customer = await UtilizatorService.showUser(rol);

        res.json(customer);
    } catch (err) {
        next(err);
    }
});


//4

router.delete('/delete/:customer_id',  async (req, res, next) => {
    const {
      customer_id
    } = req.params;

    try {
    await UtilizatorService.deleteById(parseInt(customer_id));
    res.status(204).end();

    } catch (err) {
        next(err);
    }
});



// 5
// aici am mai pus si rol chiar daca am functie separata pentru asta ca sa nu apelez 2 functii in caz
//ca vreau sa schimb rolul
router.put('/updateuser/:customer_id', async (req, res, next) => {
    const {
      customer_id
    } = req.params;
    const {
        email,
        first_name,
        username,
        rol,
        "GDPR"
    } = req.body;

    try {

        await UtilizatorService.updateUser(parseInt(customer_id), email, first_name, username, rol, "GDPR");
        res.status(204).end();
    } catch (err) {
        next(err);
    }
});




//6
router.put('/updatepass/:customer_id', async (req, res, next) => {
    const {
      customer_id
    } = req.params;
    const {
      passw
    } = req.body;

    try {

        await UtilizatorService.updatePassById(parseInt(customer_id), passw);
        res.status(204).end();
    } catch (err) {
        next(err);
    }
});












module.exports = router;
